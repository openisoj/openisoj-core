package org.bitbucket.openisoj.exceptions;

public class FieldDescriptorException extends RuntimeException {
	public FieldDescriptorException(String message) {
		super(message);
	}
}
